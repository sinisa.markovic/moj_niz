#ifndef mojniz_h
#define mojniz_h
#include <initializer_list>
#include <algorithm>
#include <iostream>
template <typename Type>
class MojNiz
{
  size_t size_ = 0;
  size_t capacity_ = 1;
  Type* arr_ = new Type[capacity_];
  void reallocate(size_t new_capacity);
  public:
  MojNiz()
  : arr_{new Type[capacity_]},
    size_{0},
    capacity_{1}
  {
  }
  MojNiz(const MojNiz& other)
  : size_{other.size_},
    capacity_{other.capacity_},
    arr_{new Type[capacity_]}
  {
    std::copy(other.arr_,other.arr_ + size_,arr_);
  }
  MojNiz(MojNiz&& other)
  : size_{other.size_},
    capacity_{other.capacity_},
    arr_{other.arr_}
  {
    other.arr_ = nullptr;
    other.size_ = 0;
    other.capacity_ = 1;
  }
  MojNiz(const std::initializer_list<Type>& init_list) 
  : size_{init_list.size()},
    capacity_{size_},
    arr_{new Type[capacity_]}
  {
    std::copy(init_list.begin(),init_list.end(),arr_);
  }
  template <typename OtherType>
  MojNiz(const MojNiz<OtherType>& other)
  : size_{other.size()},
    capacity_{other.capacity()},
    arr_{new Type[capacity_]}
  {
    std::copy(other.begin_ptr(),other.end_ptr(),arr_);
  }
  ~MojNiz()
  {
    delete [] arr_;
  }
  template <typename OtherType>
  MojNiz& operator=(const MojNiz<OtherType>& other)
  {
    size_ = other.size();
    capacity_ = other.capacity();
    delete [] arr_;
    arr_ = new Type[capacity_];
    std::copy(other.begin_ptr(),other.end_ptr(),arr_);
    return *this;
  }
  MojNiz& operator=(const MojNiz& other)
  {
    size_ = other.size_;
    capacity_ = other.capacity_;
    delete [] arr_;
    arr_ = new Type[capacity_];
    std::copy(other.arr_,other.arr_ + size_,arr_);
    return *this;
  }
  MojNiz& operator=(MojNiz&& other)
  {
    size_ = other.size_;
    capacity_ = other.capacity_;
    delete [] arr_;
    arr_ = other.arr_;
    other.arr_ = nullptr;
    other.size_ = 0;
    other.capacity_ = 1;
    return *this;
  }
  const Type* begin_ptr() const
  {
    return arr_;
  }
  const Type* end_ptr() const
  {
    return arr_ + size_;
  }
  Type* begin_ptr() 
  {
    return arr_;
  }
  Type* end_ptr() 
  {
    return arr_ + size_;
  }
  Type& operator[](size_t index)
  {
    return arr_[index];
  }
  const Type& operator[](size_t index) const
  {
    return arr_[index];
  }
  size_t size() const
  {
    return size_;
  }
  Type& at(size_t index)
  {
    if(index >= size_)
    {
      throw std::out_of_range("exception: out of range\ngenerated at: method at of MojNiz container");
    }
    return arr_[index];
  }
  const Type& at(size_t index) const
  {
    if(index >= size_)
    {
      throw std::out_of_range("exception: out of range\ngenerated at: method at of MojNiz container");
    }
    return arr_[index];
  }
  MojNiz operator++(int)
  {
    auto return_val = *this;
    for(size_t i = 0; i < size_; ++i)
    {
      ++arr_[i];
    }
    return return_val;
  }
  MojNiz& operator++()
  {
    for(size_t i = 0; i < size_; ++i)
    {
      ++arr_[i];
    }
    return *this;
  }
  Type& front()
  {
    return arr_[0];
  }
  const Type& front() const
  {
    return arr_[0];
  }
  Type& back()
  {
    return arr_[size_ - 1];
  }
  size_t capacity() const
  {
    return capacity_;
  }
  const Type& back() const
  {
    return arr_[size_ - 1];
  }
  
  void push_back(Type val);
  MojNiz operator+(const MojNiz& other) const;
  MojNiz operator*(double scalar) const;
  void pop_back();
  template <typename OtherType>
  auto operator+(const MojNiz<OtherType>& other) const;
};
template <typename Type>
template <typename OtherType>
auto MojNiz<Type>::operator+(const MojNiz<OtherType>& other) const
{
  if(other.size() != size_)
  {
    throw std::invalid_argument("exception: invalid argument\ngenerated at: method operator+ of MojNiz class\nmessage: operator+ with arbitrary input type");
  }
  MojNiz<decltype(operator[](0) + other[0])> new_instance;
  for(size_t i = 0; i < size_; ++i)
  {
    new_instance.push_back(operator[](i) + other[i]);
  }
  return new_instance;
}
template <typename Type>
MojNiz<Type> MojNiz<Type>::operator+(const MojNiz& other) const
{
  if(size_ != other.size())
  {
    throw std::invalid_argument("exception: invalid argument\ngenerated at: method operator+ of MojNiz\nmessage: containers are not the same size");
  }
  MojNiz new_instance = *this;
  for(size_t i = 0; i < size_; ++i)
  {
    new_instance[i] += other[i];
  }
  return new_instance;
}
template <typename Type>
MojNiz<Type> MojNiz<Type>::operator*(double scalar) const
{
  MojNiz new_instance = *this;
  for(size_t i = 0; i < size_; ++i)
  {
    new_instance[i] *= scalar;
  }
  return new_instance;
}
template <typename Type>
void MojNiz<Type>::push_back(Type val)
{
  if(size_ >= capacity_)
  {
    reallocate(capacity_ * 2);
  }
  ++size_;
  arr_[size_ - 1] = val;
}
template <typename Type>
void MojNiz<Type>::reallocate(size_t new_capacity)
{
  auto new_arr = new Type[new_capacity];
  capacity_ = new_capacity;
  std::copy(arr_,arr_ + size_,new_arr);
  delete [] arr_;
  arr_ = new_arr;
}
template <typename Type>
void MojNiz<Type>::pop_back()
{
  --size_;
}
#endif