#ifndef mojnizint_h
#define mojnizint_h
#include <initializer_list>
#include <algorithm>
#include <iostream>
class MojNizInt
{
  size_t size_ = 0;
  size_t capacity_ = 1;
  int* arr_ = new int[capacity_];
  void reallocate(size_t new_capacity);
  public:
  MojNizInt()
  : arr_{new int[capacity_]},
    size_{0},
    capacity_{1}
  {
  }
  MojNizInt(const MojNizInt& other)
  : size_{other.size_},
    capacity_{other.capacity_},
    arr_{new int[capacity_]}
  {
    std::copy(other.arr_,other.arr_ + size_,arr_);
  }
  MojNizInt(MojNizInt&& other)
  : size_{other.size_},
    capacity_{other.capacity_},
    arr_{other.arr_}
  {
    other.arr_ = nullptr;
    other.size_ = 0;
    other.capacity_ = 1;
  }
  MojNizInt(const std::initializer_list<int>& init_list) 
  : size_{init_list.size()},
    capacity_{size_},
    arr_{new int[capacity_]}
  {
    std::copy(init_list.begin(),init_list.end(),arr_);
  }
  ~MojNizInt()
  {
    delete [] arr_;
  }
  MojNizInt& operator=(const MojNizInt& other)
  {
    size_ = other.size_;
    capacity_ = other.capacity_;
    delete [] arr_;
    arr_ = new int[capacity_];
    std::copy(other.arr_,other.arr_ + size_,arr_);
    return *this;
  }
  MojNizInt& operator=(MojNizInt&& other)
  {
    size_ = other.size_;
    capacity_ = other.capacity_;
    delete [] arr_;
    arr_ = other.arr_;
    other.arr_ = nullptr;
    other.size_ = 0;
    other.capacity_ = 1;
    return *this;
  }
  int& operator[](size_t index)
  {
    return arr_[index];
  }
  const int& operator[](size_t index) const
  {
    return arr_[index];
  }
  size_t size() const
  {
    return size_;
  }
  int& at(size_t index)
  {
    if(index >= size_)
    {
      throw std::out_of_range("exception: invalid argument\ngenerated at: method at of MojNizInt container");
    }
    return arr_[index];
  }
  const int& at(size_t index) const
  {
    if(index >= size_)
    {
      throw std::out_of_range("exception: invalid argument\ngenerated at: method at of MojNizInt container");
    }
    return arr_[index];
  }
  MojNizInt operator++(int)
  {
    auto return_val = *this;
    for(size_t i = 0; i < size_; ++i)
    {
      ++arr_[i];
    }
    return return_val;
  }
  MojNizInt& operator++()
  {
    for(size_t i = 0; i < size_; ++i)
    {
      ++arr_[i];
    }
    return *this;
  }
  int& front()
  {
    return arr_[0];
  }
  const int& front() const
  {
    return arr_[0];
  }
  int& back()
  {
    return arr_[size_ - 1];
  }
  size_t capacity() const
  {
    return capacity_;
  }
  const int& back() const
  {
    return arr_[size_ - 1];
  }
  
  void push_back(int val);
  MojNizInt operator+(const MojNizInt& other) const;
  MojNizInt operator*(double scalar) const;
  void pop_back();
};

MojNizInt MojNizInt::operator+(const MojNizInt& other) const
{
  if(size_ != other.size())
  {
    throw std::invalid_argument("exception: invalid_argument\ngenerated at: method operator+ of MojNizInt\nmessage: containers are not the same size");
  }
  MojNizInt new_instance = *this;
  for(size_t i = 0; i < size_; ++i)
  {
    new_instance[i] += other[i];
  }
  return new_instance;
}
MojNizInt MojNizInt::operator*(double scalar) const
{
  MojNizInt new_instance = *this;
  for(size_t i = 0; i < size_; ++i)
  {
    new_instance[i] *= scalar;
  }
  return new_instance;
}
void MojNizInt::push_back(int val)
{
  if(size_ >= capacity_)
  {
    reallocate(capacity_ * 2);
  }
  ++size_;
  arr_[size_ - 1] = val;
}
void MojNizInt::reallocate(size_t new_capacity)
{
  auto new_arr = new int[new_capacity];
  capacity_ = new_capacity;
  std::copy(arr_,arr_ + size_,new_arr);
  delete [] arr_;
  arr_ = new_arr;
}
void MojNizInt::pop_back()
{
  --size_;
}
#endif